/**
* This implements the algorithms, sort function (quicksort algorithm),
* load, and print function.
*
* @authors Jance Myers, Taylor Lookabaugh
* @date 30 April 2016
* @info Course COP4534
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "algorithms.h"


/*FIRST FIT ALGORTHM STARTS HERE */
void FirstFit(Package_Data *Pkg_Data,Bin_Data *Bin){
    int count;
    Bin->bins_used=1;
    Bin_Data *Current;
    Bin->space_avail = (float) Pkg_Data->bin_size; //Sets the bin size
    Bin->contents = malloc(Pkg_Data->num_pkgs *sizeof(float)); //allocates the size of the container to the max pkgs
    for (count = 0; count < Pkg_Data->num_pkgs; count++)
    {
        Current = Bin;
        if(Pkg_Data->package_list[count] > Pkg_Data->bin_size)
        {
            continue;
        }
        if(Current->space_avail - Pkg_Data->package_list[count] >= 0)
        { //places the package if space is available
            Current->space_avail -= Pkg_Data->package_list[count]; //Update space available
            Current->contents[FindEmptySlot(Current->contents,Pkg_Data->num_pkgs)] = Pkg_Data->package_list[count]; //copies value
        }
        else if(Current->space_avail - Pkg_Data->package_list[count] <= 0)
        { //If there isn't enough space left in bin
            do
            {
                if(Current->Next_Bin == NULL)
                { //checks to see if another bin needs to be created
                    Current->Next_Bin = malloc(sizeof(Bin_Data)); //creates new bin
                    Current = Current->Next_Bin; //sets current bin to new bin
                    Current->space_avail = Pkg_Data->bin_size; //sets the space available
                    Current->contents = malloc(Pkg_Data->num_pkgs * sizeof(float)); //allocates enough space
                    Current->Next_Bin = NULL; //Sets next bin pointer to NULL for SnGs.
                    Bin->bins_used++; //increased the bins used count
                }
                else if(Current->Next_Bin != NULL)
                {
                    Current = Current->Next_Bin;
                }
            }while(Current->space_avail - Pkg_Data->package_list[count] <= 0); //Search for new bin while pkg can't fit
            
                Current->space_avail -= Pkg_Data->package_list[count]; //Update space available
                Current->contents[FindEmptySlot(Current->contents,Pkg_Data->num_pkgs)] = Pkg_Data->package_list[count]; //copies value
        }    
    }
}
/*NEXT FIT ALGORITHM STARTS HERE */
void NextFit(Package_Data *Pkg_Data,Bin_Data *Bin)
{
    int count;
    Bin->bins_used=1;
    Bin_Data *Current;
    Bin->space_avail = (float) Pkg_Data->bin_size ; //Sets the bin size
    Bin->contents = malloc(Pkg_Data->num_pkgs *sizeof(float)); //allocates the size of the container to the max pkgs 
    Current = Bin;
    for (count = 0; count < Pkg_Data->num_pkgs; count++)
    {
        if(Pkg_Data->package_list[count] > Pkg_Data->bin_size)
        {
            continue;
        }
        if((Current->space_avail - Pkg_Data->package_list[count]) > 0)
        { //places the package if space is available
            Current->space_avail -= Pkg_Data->package_list[count]; //Update space available
            Current->contents[FindEmptySlot(Current->contents,Pkg_Data->num_pkgs)] = Pkg_Data->package_list[count];//copies value
        }
        else
        { //If there isn't enough space left in bin
            Current->Next_Bin = malloc(sizeof(Bin_Data)); //creates new bin
            Current = Current->Next_Bin; //sets current bin to new bin
            Current->space_avail = (float) Pkg_Data->bin_size ; //sets the space available
            Current->contents = malloc(Pkg_Data->num_pkgs * sizeof(float)); //allocates enough space
            Current->Next_Bin = NULL; //Sets next bin pointer to NULL for SnGs.
            Bin->bins_used++; //increased the bins used count
            Current->space_avail -= Pkg_Data->package_list[count]; //Update space available
            Current->contents[FindEmptySlot(Current->contents,Pkg_Data->num_pkgs)] = Pkg_Data->package_list[count]; //copies value
        }    
    }
}

/*BEST FIT ALGORITHM STARTS HERE*/
void BestFit(Package_Data *Pkg_Data,Bin_Data *Bin)
{
    int count,count2,bins_visited=0;
    float remainder=99999;
    Bin->bins_used=1;
    Bin_Data *Current,*Best;
    Bin->space_avail = (float) Pkg_Data->bin_size +.001; //Sets the bin size
    Bin->contents = malloc(Pkg_Data->num_pkgs *sizeof(float)); //allocates the size of the container to the max pkgs
    Current = Bin;
    for (count = 0; count < Pkg_Data->num_pkgs-1; remainder = 99999,bins_visited = 0,Current = Bin,count++)
    {
        if(Pkg_Data->package_list[count] > Pkg_Data->bin_size)
        {
            continue;
        }
        for(count2=0;count2 < Bin->bins_used;count2++)
        {
            if((Current->space_avail) - Pkg_Data->package_list[count] >=  0.0001f)
            {//checks bin if space is available for package
                if(fmodf(Current->space_avail,Pkg_Data->package_list[count]) <= remainder)
                {
                //if((Current->space_avail-(Current->space_avail+Pkg_Data->package_list[count])) <= remainder){ //Checks if it fits here best.
                    Best = Current;
                    remainder = fmodf(Current->space_avail,Pkg_Data->package_list[count]);
                }
            }
            if(Current->Next_Bin != NULL && bins_visited != Bin->bins_used)
            {
                Current = Current->Next_Bin;
                bins_visited++;
            }
        }
        if ( Current->space_avail - Pkg_Data->package_list[count] < 0.0)
        { //Bin w/ enough space was not found
                if(Current->Next_Bin == NULL)
                { //creates another bin
                    Current->Next_Bin = malloc(sizeof(Bin_Data)); //creates new bin
                    Current = Current->Next_Bin; //sets current bin to new bin
                    Current->space_avail = Pkg_Data->bin_size +.001; //sets the space available
                    Current->contents = malloc(Pkg_Data->num_pkgs * sizeof(float)); //allocates enough space
                    Current->Next_Bin = NULL; //Sets next bin pointer to NULL for SnGs.
                    Bin->bins_used++; //increased the bins used count
                    Current->space_avail -= Pkg_Data->package_list[count]; //Update space available
                    Current->contents[0] = Pkg_Data->package_list[count]; //copies value
                    continue;
                }
        }
        if(remainder != 99999)
        {
            //Current = Best;//Sets the best value 
            Best->space_avail -= Pkg_Data->package_list[count]; //Update space available
            Best->contents[FindEmptySlot(Best->contents,Pkg_Data->num_pkgs)] = Pkg_Data->package_list[count]; //copies value
        }
    }    
}

/*UTILITIES START HERE */
void LoadData(Package_Data *Pkg_Data, char *argv[])
{
    int count;
    fpos_t file_position;
    FILE *data_file = fopen(argv[1],"r"); //opening and reading file
    if (data_file == NULL)
    { //verifies file can be opened
        printf("Cannot open \"theItems.txt\". Exiting.\n"); //message to user the file can't be opened
        exit(1); //quits
    }
    fscanf(data_file,"%d", &Pkg_Data->bin_size); //scans in the size of the container
    fgetpos(data_file,&file_position); //Saving stream position
    CountPkgs(data_file,Pkg_Data); //Getting amount of packages
    fsetpos(data_file,&file_position);
    Pkg_Data->package_list = malloc(Pkg_Data->num_pkgs*sizeof(float)); //allocates enough spaces in package list
    for(count = 0; count < Pkg_Data->num_pkgs;count++)
        fscanf(data_file,"%f", &Pkg_Data->package_list[count]);
}

int FindEmptySlot(float *array,int max_pos)
{
    int count;
    for(count = 0; count < max_pos; count++)
        if(array[count] == 0)
            return count;
    return -1;
}


void PrintResults(Package_Data *Pkg_Data,Bin_Data *Online_Bin,Bin_Data *Offline_Bin)
{
    int count,count2,count3,count4;
    float sum;
    Bin_Data *Outer_Bin,*Inner_Bin;
    printf("╔═════════╦═════════════════╗\n");
    printf("║  Policy ║ Total Bins Used ║\n");
    printf("╠═════════╩═════════════════╣\n");
    printf("║      ONLINE ALGORITHM     ║\n");
    printf("╠═════════╦═════════════════╣\n");
    printf("║First Fit║       %3d       ║\n",Online_Bin[0].bins_used);
    printf("╠═════════╬═════════════════╣\n");
    printf("║Next Fit ║       %3d       ║\n",Online_Bin[1].bins_used);
    printf("╠═════════╬═════════════════╣\n");
    printf("║Best Fit ║       %3d       ║\n",Online_Bin[2].bins_used);
    printf("╠═════════╩═════════════════╣\n");
    printf("║     OFFLINE ALGORITHM     ║\n");
    printf("╠═════════╦═════════════════╣\n");
    printf("║First Fit║       %3d       ║\n",Offline_Bin[0].bins_used);
    printf("╠═════════╬═════════════════╣\n");
    printf("║Best Fit ║       %3d       ║\n",Offline_Bin[2].bins_used);
    printf("╚═════════╩═════════════════╝\n");
    
    for(count = 0; count < 2;count++)
    { //Policy counter
        if(count == 0)
        {
             Outer_Bin = Online_Bin;
             printf("\n\nThe Bins for the Online Algorithm are as follows:\n");
        }
        if(count == 1)
        {
            Outer_Bin = Offline_Bin;
            printf("\n\nThe Bins for the Offline Algorithm are as follows:\n");
        }
        for(count2 = 0; count2 < 3;count2++)
        { //Type fit counter
                Inner_Bin = &Outer_Bin[count2];
                if(count2 == 0)
                {
                    printf("\nFirst Fit results are as follows:\n");
                }
                if(count2 == 1)
                {
                    if (&Outer_Bin[count2] == &Offline_Bin[1])
                        continue;
                    printf("\nNext Fit results are as follows:\n");
                }
                if(count2 == 2)
                {
                    printf("\nBest Fit results are as follows:\n");
                }
            for(count3 = 0; count3 < Outer_Bin[count2].bins_used;count3++)
            { //rotates bins
                printf("\nBin %d contains:\n",count3+1);
                for(count4 = 0,sum=0; count4 < Pkg_Data->num_pkgs;count4++) //prints individual elements
                    if(Inner_Bin->contents[count4] != 0 )
                    {
                        printf("[%.2f]",Inner_Bin->contents[count4]);
                        sum+= Inner_Bin->contents[count4];
                    }
                printf("\n");
                printf("Total weight in bin %d is %.2f\n\n",count3+1,sum);
                if(Inner_Bin->Next_Bin != NULL)
                    Inner_Bin = Inner_Bin->Next_Bin;
            }
        }
    }
    
    for(count = 0,count2 = 0; count < Pkg_Data->num_pkgs;count++)
        if(Pkg_Data->package_list[count] > Pkg_Data->bin_size)
        {
            count2++;
            if(count2 != 0)
                printf("\n\nPackages that were found to be oversized:\n");
            printf("[%.2f]",Pkg_Data->package_list[count]);
        }
     printf("\n\n");
    
}
void QuickSort(float *array, int base, int max)
{
      int left = base, right = max; //set variables
      float temp; //set temp variable
      float pivot = array[(base + max) / 2]; //set pivot value
      
      while (left <= right)
      { //loop to find values higher/lower than pivot
          while (array[left] > pivot)
              left++;
          while (array[right] < pivot)
              right--;
          if (left <= right)
          { //swap values if lower than pivot
              temp = array[left];
              array[left] = array[right];
              array[right] = temp;
              left++;
              right--;
          }
      };
      if (base < right) //recursion to continue searching elements if not all values have been checked
            QuickSort(array, base, right);
      if (left < max)
            QuickSort(array, left, max);
}

void CountPkgs(FILE *data_file,Package_Data *Pkg_Data)
{
    while(!feof(data_file))
    {
        if(fgetc(data_file) == '\n')
            Pkg_Data->num_pkgs++;
    }
    Pkg_Data->num_pkgs--;
}
