/**
* This program runs greedy algorithms for binPacking. packs items into bins of fixed size.
* Values are parsed from a text file
*
* @authors Jance Myers, Taylor Lookabaugh
* @date 30 April 2016
* @info Course COP4534
*/

#include "algorithms.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if(argc > 1)
    {
        Bin_Data *Online_Bin = malloc(3*sizeof(Bin_Data)); //Creating strcture for the 3 types of sorting methods
        Package_Data *Pkg_Data = malloc(sizeof(Package_Data)); //Creating the struct to hold package values after first read.
        Bin_Data *Offline_Bin = malloc(3*sizeof(Bin_Data)); //Creating strcture for the 3 types of sorting methods
        LoadData(Pkg_Data, argv); //Loads package file into memory
        FirstFit(Pkg_Data,&Online_Bin[0]);
        NextFit(Pkg_Data,&Online_Bin[1]);
        BestFit(Pkg_Data,&Online_Bin[2]);
        QuickSort(Pkg_Data->package_list,0,Pkg_Data->num_pkgs-1);
        FirstFit(Pkg_Data,&Offline_Bin[0]);
        BestFit(Pkg_Data,&Offline_Bin[2]);
        PrintResults(Pkg_Data,Online_Bin,Offline_Bin);
    }
    else
        printf("./binPacking <input_file>\n");
    return 0;
}
