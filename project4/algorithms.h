/**
* This declares the algorithms, sort, print, load functions.
* also declares structs for bin and packages.
*
* @authors Jance Myers, Taylor Lookabaugh
* @date 30 April 2016
* @info Course COP4534
*/

#ifndef algorithms
#define algorithms
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


struct Bin_Data{ //bin data structure
    int bins_used; //amount of bins used (only used for head bin)
    float space_avail; //space available for current bin
    float *contents; //contents of bin
    struct Bin_Data *Next_Bin; //link to child bin
};
typedef struct Bin_Data Bin_Data;

struct Package_Data{ //package data structure
    int bin_size; //bin size
    int num_pkgs; //number of packages
    float *package_list; //package list
    
};
typedef struct Package_Data Package_Data;
/**
 * @brief CountPkgs
 * calculates number of total packages in the text file to use in the algorithms.
 * @param data_file
 * pointer to file.
 * @param Pkg_Data
 * the packages are in this data structure.
 */
void CountPkgs(FILE *data_file,Package_Data *Pkg_Data); //counts the number of packages. Used for creating arrays
/**
 * @brief LoadData
 * parses values from text file into data structure.
 * @param Pkg_Data
 * data structure used to load into list of array.
 */
void LoadData(Package_Data *Pkg_Data, char *[]); //loads the package list into memory from file
/**
 * @brief FirstFit
 * algorithm for first fit in packing bins.
 * @param Pkg_Data
 * data structure packages
 * @param Bin
 * data structure bins
 */
void FirstFit(Package_Data *Pkg_Data,Bin_Data *Bin); //first fit algorithm
/**
 * @brief NextFit
 * next fit algorithm to pack items into bins.
 * @param Pkg_Data
 * @param Bin
 */
void NextFit(Package_Data *Pkg_Data,Bin_Data *Bin); //next fit algorithm
/**
 * @brief FindEmptySlot
 * finds an empty slot and used to place items into bins.
 * @param array
 * @param max_pos
 * @return
 * whether there is an empty slot available or not.
 */
int FindEmptySlot(float *array,int max_pos); //Finds empty slot in array, used to place packages into bins
/**
 * @brief BestFit
 * best fit algorithm to pack items into bins.
 * @param Pkg_Data
 * @param Bin
 */
void BestFit(Package_Data *Pkg_Data,Bin_Data *Bin); //Best fit algorithm
/**
 * @brief PrintResults
 * prints the results of the respective algorithms.
 * table showing how many bins were used for respective algorithm
 * and the data in the structures.
 * @param Pkg_Data
 * @param Online_Bin
 * @param Offline_Bin
 */
void PrintResults(Package_Data *Pkg_Data,Bin_Data *Online_Bin,Bin_Data *Offline_Bin); //prints results 
/**
 * @brief QuickSort
 * quicksort algorithm to sort packages for
 * offline algorithms.
 * @param array
 * @param base
 * @param max
 */
void QuickSort(float *array, int base, int max); //Sorts package data max first for offline sorting.

#endif
