/**

 * This program finds the solution to the TSP problem using
 * a brute force approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h> //clock_gettime()
#include <stdint.h>
#include "input.h"
#include "time.h"
#include "output.h"


int main(int argc, char *argv[])
{
	int cityNum; //hold value for numbers of cities to run through
	int cWeight[15][15]; // assumed 15x15 matrix for 108 lines from cityWeight.txt
	BF_Data *Data = (BF_Data*) malloc(sizeof(BF_Data));

	if (argv[2] != NULL)
		cityNum = strtol(argv[2], NULL, 0);
	else
		cityNum = 0;

	if (argc == 3 && (cityNum > 1 && cityNum <= 15))
	{

		InitWeight(cWeight); // zero out the 2d array matrix
		CreateWeight(cWeight, argv); //insert the numbers from the file based on city (A,B,2)
		Print_Matrix(cWeight, cityNum);

		int *choice_perm = malloc(cityNum * sizeof(int)); 	//create permute order array
		int *best_perm = malloc(cityNum * sizeof(int)); 	//use to keep best cost perm order.

		basePermOrder(choice_perm, cityNum); //create base order

		printf("\n");

		clock_gettime(CLOCK_MONOTONIC, &Data->ts_start);
		permute(cWeight, choice_perm, best_perm, 1, cityNum - 1); // brute force function
		clock_gettime(CLOCK_MONOTONIC, &Data->ts_end);

		Print_BF_Results(best_perm, cityNum - 1, Data);
	}

	else
	{
		printf("Wrong parameters! \n .... ex ./tspBruteForce cityWeights.txt 10\n");
	}

	return 0;
}
