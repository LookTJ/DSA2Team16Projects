/**

 * This program finds the solution to the TSP problem using
 * a genetic approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#ifndef algorithm
#define algorithm

/*
 * structure that contains genetic information
 *
 * @param weights 2d integer array containing 15x15 elements that holds the distance between cities
 * @param current_permutation array that holds the current value of distance between cities
 * @param num_cities the number of cities to travel between
 * @param num_tours the number of attempts at finding the shortest path
 * @param num_gentours counter that keeps track of tours
 * @param num_comp used to keep track of random generations
 * @param num_gen number of generations
 * @param num_mut number of mutations
 * @param tour_list list of all permutations in current generation
 * @param tour_weight list of elite weights
 * @param timeval start recording start time
 * @param timeval stop recording stop time
 */
struct Gen_Data{
    int weights[15][15];
    int *current_permutation;
    int num_cities;
    int num_tours;
    int num_gentours;
    int num_comp;
    int num_gen;
    int num_mut;
    int **tour_list;
    double *tour_weight;
    struct timeval start;
    struct timeval stop;
};
typedef struct Gen_Data Gen_Data;

/*
 * Driver for genetic algorithm
 *
 * @param GData structure containing all genetic information
 */
void Gen_Alg(Gen_Data *GData);


#endif
