/**
 * This declares input functions and bruteforce functions
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#ifndef input
#define input

//struct to keep time values
struct BF_Data
{
    struct timespec ts_start;
    struct timespec ts_end;
};
typedef struct BF_Data BF_Data;

/**
 * Initializes the matrix's weights to zero.
 *
 * @param cWeight 2d array representing distance between cities
 */
void InitWeight(int [15][15]);

/**
 * Parses values from a text file into its proper row/col.
 *
 * @param cWeight 2d array representing distance between cities
 * @param argv used to pass the name of the text file
 */
void CreateWeight(int [15][15], char *argv[]);

/**
 * Creates a base permutation order
 *
 * @param choicePerm base permutation order
 * @param cityNum number of cities to permutate
 */
void basePermOrder(int*,int); //creates permutation order

/**
 * Swaps the two values
 *
 * @param First value to be swapped
 * @param Second value to be swapped
 */
void swap(int*, int*); // swap values

/**
 * an algorithm used to find a solution for bruteforce method.
 *
 * @param Array of distances
 * @param Array of city choices
 * @param Array of best route
 * @param Number of cities
 */
void permute(int [15][15], int*,int*, int, int); // bruteforce alg

/*
 * Calculates and keeps best weight and best permutation order
 *
 * @param Array of distances
 * @param Array of city choices
 * @param Array of best route
 * @param Number of cities
 *
 * @return best the total cost of the order
 */
int calcWeight(int [15][15], int*,int*, int); // calculates and keeps best weight and best permutation order.

/**
 * prints the results of the bruteforce solution, the best path, and time it took
 * to calculate the solution
 *
 * @param bestPerm the total distance between cities
 * @param city number of cities
 * @param Data structure containing time information
 */
void Print_BF_Results(int *, int, BF_Data *); //print results for bruteforce alg with cost, path, and time

#endif
