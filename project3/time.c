/**
 * This implements the function for printing how much time it
 * took to do brute force method
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "time.h"


void print_BFTime(BF_Data *Data)
{
	//calculates  and prints elasped time.
	if ((Data->ts_end.tv_sec - Data->ts_start.tv_sec) / 60 > 0)
	{
		printf("%d minutes, ",
				(int) (Data->ts_end.tv_sec - Data->ts_start.tv_sec) / 60);

		if ((Data->ts_end.tv_sec - Data->ts_start.tv_sec) % 60 != 0)
		{
			printf("%d seconds, and ",
					(int) (Data->ts_end.tv_sec - Data->ts_start.tv_sec) % 60);
		}
	}

	else if ((Data->ts_end.tv_sec - Data->ts_start.tv_sec) % 60 != 0)
	{
		printf("%d second(s) and ",
				(int) (Data->ts_end.tv_sec - Data->ts_start.tv_sec));
	}

	if (Data->ts_end.tv_nsec > Data->ts_start.tv_nsec)
	{
		printf("%.3lf millisec(s) ",
				(double) (Data->ts_end.tv_nsec - Data->ts_start.tv_nsec)
						/ 1000000);
	}

	else
	{
		printf("%.3lf millisec(s) ",
				(double) (Data->ts_start.tv_nsec - Data->ts_end.tv_nsec)
						/ 1000000);
	}
}
