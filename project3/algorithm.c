/**

 * This program finds the solution to the TSP problem using
 * a genetic approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "algorithm.h"
#include "tools.h"

void Gen_Alg(Gen_Data *GData)
{
	int count, count2, count3, randcount, tour_start;

	CopyToCurrent(GData, 0);

	for (count = 0; count < GData->num_gen; count++)
	{
		if (count == 0)
		{
			tour_start = 2;
		}

		else
		{
			tour_start = (GData->num_tours - 1) - GData->num_mut;
		}

		for (count2 = tour_start; count2 < GData->num_tours; count2++)
		{
			if (rand() % 100 < rand() % 5)
			{
				CrossOver(GData, count2);
				Calc_GWeight(GData, count2);
				continue;
			}

			if (count2 >= tour_start / 2)
			{
				if (count2 == tour_start / 2)
				{
					CopyToCurrent(GData, 1);
				}

				randcount = (int) rand() % GData->num_cities - 1;

				for (count3 = 0; count3 < randcount; count3++)
				{
					SwapElement(
							&GData->current_permutation[rand()
									% GData->num_cities],
							&GData->current_permutation[rand()
									% GData->num_cities]);
				}

				CopyToList(GData, count2);
				Calc_GWeight(GData, count2);
			}

			else
			{
				randcount = (int) rand() % GData->num_cities - 1;

				for (count3 = 0; count3 < randcount; count3++)
				{
					SwapElement(
							&GData->current_permutation[rand()
									% GData->num_cities],
							&GData->current_permutation[rand()
									% GData->num_cities]);
				}

				CopyToList(GData, count2);
				Calc_GWeight(GData, count2);
			}
		}
		SortTours(GData);
	}
}
