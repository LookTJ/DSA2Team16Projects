/**

 * This program finds the solution to the TSP problem using
 * a genetic approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#ifndef tools
#define tools

/*
 * Swaps two elements in an array
 *
 * @param fro first element to swap
 * @param to second element to swap
 */
void SwapElement(int *fro,int *to); //Swaps two elements in array;

/*
 * Function that seeds the weight list
 *
 * @param GData structure containing all genetic information
 * @param argv array containing command line arguments
 */
void Seed_Weight(Gen_Data *GData, char *argv[]);

/*
 * Function that seeds the base permutations
 *
 * @param GData structure containing all genetic information
 */
void Seed_BasePerm(Gen_Data *GData);

/*
 * Checks route to verify city hasn't been visited
 *
 * @param GData structure containing all genetic information
 * @param city city to be checked
 * @param slot row value of city to be checked
 *
 * @return 1 if previous city
 * @return 0 if no path found
 */
int CheckIfOnRoute(Gen_Data *GData,int city,int slot);

/*
 * Calculates and processes weight for genetic algorithm
 *
 * @param GData structure containing all genetic information
 * @param tour summed value of cities visited
 */
void Calc_GWeight(Gen_Data *GData,int tour);

/*
 * Prints the time of the genetic algorithm
 *
 * @param GData structure containing all genetic information
 */
void Print_GTime(Gen_Data *GData);

/*
 * Prints results of the genetic algorithm
 *
 * @param GData structure containing all genetic information
 */
void Print_GResults(Gen_Data *GData);

/*
 * Sorts tours of the genetic algorithm
 *
 * @param GData structure containing all genetic information
 */
void SortTours(Gen_Data *GData);

/*
 * Copies current permutation to genetic tour list
 *
 * @param GData structure containing all genetic information
 * @param tour sum of cities visited
 */
void CopyToList(Gen_Data *GData,int tour);

/*
 * Copies a genetic tour to the current permutation array
 *
 * @param GData structure containing all genetic information
 * @param tour sum of cities visited
 */
void CopyToCurrent(Gen_Data *GData,int tour);

/*
 * Prompts user for genetic algorithm input
 *
 * @param GData structure containing all genetic information
 * @param argv array containing command line arguments
 */
void GetInput(Gen_Data *GData, char *argv[]); //Prompts user for genetic algorithm input

/*
 * Swaps front of one tour with back of another
 *
 * @param GData structure containing all genetic information
 * @param slot variable marking the row of the tour to be swapped
 */
void CrossOver(Gen_Data *GData,int slot);

#endif
