/**

 * This program finds the solution to the TSP problem using
 * a genetic approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "algorithm.h"
#include "tools.h"

int main(int argc, char **argv)
{
	if (argc < 6)
	{
		printf("Error: Not enough arguments\n");
	}

	srand(time(NULL));

	Gen_Data *GData = malloc(sizeof(Gen_Data));

	GetInput(GData, argv);

	Seed_Weight(GData, argv);

	Seed_BasePerm(GData);

	gettimeofday(&GData->start, NULL);
	Gen_Alg(GData);
	gettimeofday(&GData->stop, NULL);

	Print_GResults(GData);

	return 0;
}
