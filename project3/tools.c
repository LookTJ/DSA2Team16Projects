/**

 * This program finds the solution to the TSP problem using
 * a genetic approach. Values are parsed from a text file
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "algorithm.h"
#include "tools.h"

void SwapElement(int *fro, int *to)
{
	int temp_value;
	temp_value = *fro;
	*fro = *to;
	*to = temp_value;
}

void Seed_Weight(Gen_Data *GData, char *argv[])
{
	int count = 1;
	int weightNum;
	int i, j;

	char city1, city2;
	char line[200];

	GData->tour_weight = malloc(GData->num_tours * sizeof(int*));

	for (i = 0; i < 15; ++i)
	{
		for (j = 0; j < 15; ++j)
		{
			GData->weights[i][j] = 0;
		}
	}

	FILE *my_file;
	my_file = fopen(argv[1], "r");

	if (my_file == NULL)
	{
		printf("Couldn't open file \"%s\". Exiting.\n", argv[1]);
		exit(1);
	}

	fgets(line, 100, my_file);
	
	count = 0;

	while (1)
	{
		if (count == 105)
		{
			break;
		}
		sscanf(line, "%c,%c,%d", &city1, &city2, &weightNum);

		if ((city1 >= 65 && city1 <= 79) && (city2 >= 65 && city2 <= 79))
		{
			GData->weights[city1 - 65][city2 - 65] = weightNum;
			GData->weights[city2 - 65][city1 - 65] = weightNum;
			
			for (i=0;i<15;++i)
			{
				for (j=0;j<15;++j)
				{
					printf("%2d ",GData->weights[i][j]);
				}
				printf("\n");
			}
			getchar();
			
		}

		else
		{
			printf("Line %d has an improper format.\n", count);
			printf("Format must follow A-O (all caps): A,B,7.\n");
		}

		fgets(line, 100, my_file);
		count++;
	}

	fclose(my_file);

	for (i = 0; i < 15; ++i)
	{
		for (j = 0; j < 15; ++j)
		{
			printf(" %2d", GData->weights[i][j]);
		}

		printf("\n");
	}
}

void Seed_BasePerm(Gen_Data *GData)
{
	int count, count2, to = 0, from = 0;
	int cheap_route;

	GData->current_permutation = malloc(GData->num_cities * sizeof(int));
	GData->tour_list = malloc(GData->num_tours * sizeof(int*));

	for (count = 0; count < GData->num_tours; count++)
	{
		GData->tour_list[count] = malloc(GData->num_cities * sizeof(int));
	}

	for (count = 0; count <= GData->num_cities; count++)
	{
		for (count2 = 1, cheap_route = 999999999; count2 <= GData->num_cities;
				count2++) //For loop for upto amount of cities
		{
			if (GData->weights[to][count2] <= cheap_route
					&& GData->weights[to][count2] != 0
					&& !CheckIfOnRoute(GData, count2, 0))
			{
				GData->tour_list[0][count] = count2;
				cheap_route = GData->weights[to][count2];
				to = count2;
			}
		}

	}
	Calc_GWeight(GData, 0);
	for (count = 1, cheap_route = 99999999; count <= GData->num_cities; count++)
	{
		if (cheap_route > GData->weights[count][0])
		{
			cheap_route = GData->weights[count][0];
			from = count;
		}
	}

	for (count = GData->num_cities; count >= 0; count--)
	{
		for (count2 = 1, cheap_route = 99999999; count2 <= GData->num_cities;
				count2++)
		{
			if (GData->weights[count2][from] <= cheap_route
					&& GData->weights[count2][from] != 0
					&& !CheckIfOnRoute(GData, count2, 1))
			{
				GData->tour_list[1][count] = count2;
				cheap_route = GData->weights[count2][from];
				to = count2;
			}
		}
		from = to;
	}
	Calc_GWeight(GData, 1);
}

int CheckIfOnRoute(Gen_Data *GData, int city, int slot)
{
	int count;
	for (count = 0; count < GData->num_cities; count++)
	{
		if (GData->tour_list[slot][count] == city)
		{
			return 1;
		}
	}
	return 0;
}

void Calc_GWeight(Gen_Data *GData, int tour)
{
	int count;

	GData->tour_weight[tour] = 0;

	for (count = 0; count < GData->num_cities; count++)
	{
		if (count == 0)
		{
			GData->tour_weight[tour] +=
					GData->weights[0][(GData->tour_list[tour][count])];
		}

		if (count == GData->num_cities - 1)
		{
			GData->tour_weight[tour] +=
					GData->weights[(GData->tour_list[tour][count])][0];
			continue;
		}

		GData->tour_weight[tour] +=
				GData->weights[GData->tour_list[tour][count]][GData->tour_list[tour][count
						+ 1]];
	}
}

void Print_GTime(Gen_Data *GData)
{
	if ((GData->stop.tv_sec - GData->start.tv_sec) / 60 > 0)
	{
		printf("%d minutes, ",
				(int) (GData->stop.tv_sec - GData->start.tv_sec) / 60);

		if ((GData->stop.tv_sec - GData->start.tv_sec) % 60 != 0)
		{
			printf("%d seconds, and ",
					(int) (GData->stop.tv_sec - GData->start.tv_sec) % 60);
		}
	}

	else if ((GData->stop.tv_sec - GData->start.tv_sec) != 0)
	{
		printf("%d second(s) and ",
				(int) (GData->stop.tv_sec - GData->start.tv_sec));
	}

	if (GData->stop.tv_usec > GData->start.tv_usec)
	{
		printf("%.2lf milli second(s) ",
				(double) (GData->stop.tv_usec - GData->start.tv_usec) / 1000);
	}

	else
	{
		printf("%.2lf milli second(s) ",
				(double) (GData->start.tv_usec - GData->stop.tv_usec) / 1000);
	}
}

void Print_GResults(Gen_Data *GData)
{
	int count;

	printf("Using the genetic algorithm, best path is: ");

	for (count = 0; count < GData->num_cities; count++)
	{
		printf("[%d]", GData->tour_list[0][count]);
	}

	printf(" with cost of %.0lf in ", GData->tour_weight[0]);
	Print_GTime(GData);
	printf("\n");
}

void SortTours(Gen_Data *GData)
{
	int count, count2, flag;
	int temp_tour[GData->num_cities];

	double temp_weight;

	do
	{
		for (count = 0, flag = 0; count < GData->num_tours - 1; count++)
		{
			if (GData->tour_weight[count] > GData->tour_weight[count + 1])
			{
				temp_weight = GData->tour_weight[count];
				GData->tour_weight[count] = GData->tour_weight[count + 1];
				GData->tour_weight[count + 1] = temp_weight;

				for (count2 = 0; count2 < GData->num_cities; count2++)
				{
					temp_tour[count2] = GData->tour_list[count + 1][count2];
					GData->tour_list[count + 1][count2] =
							GData->tour_list[count][count2];
					GData->tour_list[count][count2] = temp_tour[count2];
				}

				flag++;
			}
		}
	} while (flag);
}

void CopyToList(Gen_Data *GData, int tour)
{
	int count;

	for (count = 0; count < GData->num_cities; count++)
	{
		GData->tour_list[tour][count] = GData->current_permutation[count];
	}
}

void CopyToCurrent(Gen_Data *GData, int tour)
{
	int count;
	for (count = 0; count < GData->num_cities; count++)
	{
		GData->current_permutation[count] = GData->tour_list[tour][count];
	}
}

void GetInput(Gen_Data *GData, char *argv[])
{
	GData->num_cities = atoi(argv[2]);
	GData->num_gen = atoi(argv[3]);
	GData->num_tours = atoi(argv[4]);

	float value = atoi(argv[5]) / 100;
	GData->num_mut = (int) (GData->num_tours) * value;

}

void CrossOver(Gen_Data *GData, int slot)
{
	int count;
	for (count = 0; count < GData->num_cities - 1 / 2; count++)
	{
		GData->tour_list[slot][count] = GData->tour_list[slot - 1][count];
	}

	for (count = GData->num_cities - 1; count >= 0; count--)
	{
		if (!CheckIfOnRoute(GData, GData->tour_list[slot - 2][count], slot))
		{
			GData->tour_list[slot][count] = GData->tour_list[slot - 2][count];
		}
	}
}
