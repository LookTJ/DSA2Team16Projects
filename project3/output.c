/**
 * This implements output functions.
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <stdlib.h>
#include <stdio.h>
#include "input.h"
#include "output.h"

void Print_Matrix(int cWeight[15][15], int city)
{
    int i, j;
    for(i=0; i < city; ++i)
    {
        for(j=0; j<city; ++j)
        {
            printf("%2d ", cWeight[i][j]);
        }

        printf("\n");
    }
    printf("\n");
}
