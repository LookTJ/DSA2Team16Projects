/**
 * This implements input and brute force algorithm functions.
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "time.h"

int best = 999999999;

int getcity(void)
{
	char buffer[10];
	char *p;

	long int i;

	if (fgets(buffer, sizeof(buffer), stdin) != NULL)
	{
		i = strtol(buffer, &p, 10);

		if (buffer[0] != '\n' && (*p == '\n' || *p == '\0')
				&& (i > 2 && i < 21))
		{
			return (int) i;
		}

		else
		{
			return 0;
		}
	}
	return 0;
}

void InitWeight(int cWeight[15][15])
{
	int i, j;

	for (i = 0; i < 15; ++i)
	{
		for (j = 0; j < 15; ++j)
		{
			cWeight[i][j] = 0;
		}
	}

}

void CreateWeight(int cWeight[15][15], char *argv[])
{
	int weightNum;
	int cnt = 1; //counter for line.

	char line[200];
	char city1;
	char city2;

	FILE *weightfile;
	weightfile = fopen(argv[1], "r");
	cnt = 0;

	if (weightfile == NULL)
	{
		perror("Error opening text  file\n\n");
		exit(EXIT_FAILURE);
	}
	
	else
	{
		fgets(line, 100, weightfile);

		while (1)
		{
			if (cnt == 105)
			{
				break;
			}

			sscanf(line, "%c,%c,%d", &city1, &city2, &weightNum);

			if ((city1 >= 65 && city1 <= 79) && (city2 >= 65 && city2 <= 79))
			{
				cWeight[city1 - 65][city2 - 65] = weightNum;
				cWeight[city2 - 65][city1 - 65] = weightNum;
			}

			else
			{
				printf("Line %d has an improper format.\n", cnt);
				printf("Format must follow A-O (all caps): A,B,7.\n");
			}

			fgets(line, 100, weightfile);
			cnt++;
		}
	}
	fclose(weightfile);
}

void basePermOrder(int *a, int cityNum)
{
	int j;
	for (j = 0; j < cityNum; ++j)
	{
		a[j] = j;
	}
}

void swap(int *a, int *b)
{
	int temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

void permute(int b[15][15], int *a, int *c, int l, int r)
{
	if (r == l)
	{
		calcWeight(b, a, c, r);
		return;
	}

	int i = l;
	for (i = l; i <= r; ++i)
	{
		swap(a + l, a + i);
		permute(b, a, c, l + 1, r);
		;
		swap(a + l, a + i);
	}

	return;
}

int calcWeight(int a[15][15], int *b, int *c, int city)
{
	int counter;
	int current = 0;

	for (counter = 0; counter <= city; ++counter)
	{
		if (counter == 0)
		{
			current += a[0][b[counter]]; //add initial value
		}

		if (counter == city)
		{
			current += a[b[counter]][0]; //add row+col til end.
			continue;
		}

		current += a[b[counter]][b[counter + 1]];
	}

	if (best > current)
	{
		best = current;

		for (counter = 0; counter <= city; ++counter)
		{
			c[counter] = b[counter];
		}
	}

	return best;
}

void Print_BF_Results(int *a, int city, BF_Data *Data)
{
	printf("optimal cost from bruteforce: %d from path: ", best);

	int i;
	for (i = 0; i <= city; ++i)
	{
		printf("[%d] ", a[i]);
	}

	printf("\n");
	printf("time it took to bruteforce: ");
	print_BFTime(Data);
	printf("\n");
}
