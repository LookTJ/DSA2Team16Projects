/**
 * This declares the print function for time.
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#ifndef time
#define time

/**
 * prints how much time it took to find a solution for brute force method
 *
 * @param Data structure holding the start and end time.
 */
void print_BFTime(BF_Data *Data);
#endif
