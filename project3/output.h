/**
 * This declares output functions
 *
 * @authors Jance Myers, Taylor Lookabaugh
 * @date 12 April 2017
 * @info Course COP 4534
 */

#ifndef output
#define output

/**
 * prints the 2d 15x15 matrix.
 *
 * @param cWeight 2d array representing distance between cities
 * @param city number of cities
 */
void Print_Matrix(int [15][15], int); //print matrix of cities

#endif
