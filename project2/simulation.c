/**
* This implements the pseudo code for simulation.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "queue.h"
#include "simulation.h"
#include "func.h"

Customer* BuildPriorityQueue(int arrivals)
{
    int counter;
    struct Customer *current, *previous, *priorityhead;
    priorityhead = malloc(sizeof(struct Customer*));
    priorityhead->nextCust = malloc(sizeof(struct Customer*));
    priorityhead->departureTime = -1;
    priorityhead->previousCust = NULL;
    current = priorityhead->nextCust;
    previous = priorityhead;
    for(counter=1; counter < arrivals; ++counter)
    {
        current->nextCust = malloc(sizeof(struct Customer*));
        current->departureTime = -1;
        current->previousCust = previous;
        previous = current;
        current = current->nextCust;
    }
    previous->nextCust = NULL;
    return priorityhead;
}

void ProcessNextEvent(struct Queue *queuelist, float *curremt_time, float lambda, struct Servers *Servs, struct Statistics *Stats)
{
    struct Customer *priority, *fifo;
    priority = queuelist->PriorityHead;
    while(queuelist->PriorityHead != NULL)
    {
        *curremt_time += GetNextRandomInterval(lambda);
        if(queuelist->FifoHead != NULL && Servs->available < Servs->total) //Checks FIFO queue if slots are open.
        {
            queuelist->FifoHead->startofServiceTime = *curremt_time;
            queuelist->FifoHead->departureTime = *curremt_time + GetNextRandomInterval(lambda);
            RemoveFromFifo(queuelist);
            Servs->available++;
        }
        else if(priority->departureTime == -1.0) // Check for arrivals
        {
            if(Servs->available<Servs->total) //checks if server is available, one is
            {
                Servs->available++;
                priority->arrivalTime = *curremt_time;
                priority->startofServiceTime = *curremt_time;
                priority->departureTime = *curremt_time + GetNextRandomInterval(lambda);
                SortPriorty(queuelist);
            }
            else // All servers are working, place in FIFO
            {
                Stats->waited++;
                priority->arrivalTime = *curremt_time;
                RemoveFromPriority(queuelist, priority);
                if(queuelist->FifoHead == NULL)
                    queuelist->FifoHead = priority;
                else
                {
                    fifo = queuelist->FifoHead;
                    while (fifo->nextCust != NULL) {
                        fifo = fifo->nextCust;
                    }
                    fifo->nextCust = priority;
                }
            }
        }

        else // Process event for departure
        {
            if(priority->departureTime < *curremt_time)
            {
                Stats->wait += priority->startofServiceTime - priority->arrivalTime;
                Stats->service += priority->departureTime - priority->startofServiceTime;
                RemoveFromPriority(queuelist, priority);
                Servs->available--;
            }
            else
            {
                if(priority->nextCust != NULL)
                {
                    priority = priority->nextCust;
                    continue;
                }
            }
        }
        priority = queuelist->PriorityHead;
    }
}

void BuildServers (struct Queue *queuelist, Servers *servs, float lambda, float *current_time)
{
    int counter;
    Customer *priority;
    priority = queuelist->PriorityHead;
    *current_time += GetNextRandomInterval(lambda);
    for(counter=0; counter<servs->total; ++counter)
    {
        priority->arrivalTime = *current_time;
        priority->startofServiceTime = *current_time;
        priority->departureTime = *current_time + GetNextRandomInterval(lambda);
    }
    servs->available = servs->total;
    SortPriorty(queuelist);
}
