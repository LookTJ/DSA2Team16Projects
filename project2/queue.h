/**
* This declares the struct and fuctions for queues.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#ifndef queue
#define queue

typedef struct Queue
{
    struct Customer *FifoHead;
    struct Customer *PriorityHead;
} Queue;

//typedef struct Queue Queue;

/**
 * @brief SortPriorty
 * Sorts the priorty queue.
 *
 * @param queuelist the pointer to the queue in queue.h
 */
void SortPriorty(Queue *queuelist);
/**
 * @brief RemoveFromPriority
 * Remove a link from a priority queue.
 *
 * @param queuelist
 * the pointer to queue in queue.h
 *
 * @param link
 * the link to the Customer struct.
 *
 */
void RemoveFromPriority(Queue *queuelist, struct Customer *link);
/**
 * @brief RemoveFromFifo
 * Remove a link from the FIFO
 * @param queuelist
 * the queue of which the link is being removed from.
 *
 */
void RemoveFromFifo(Queue *queuelist);
#endif
