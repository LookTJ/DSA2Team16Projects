/**
* This implements the functions for generating random interval for time.
* also to get input.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "func.h"

float GetNextRandomInterval(float lambda)
{
    return (-1 * (1.0/lambda) * logf((float) rand()/RAND_MAX));
}

int getint(void)
{
    char buffer[10];
    char *p;
    long int i;

    if(fgets(buffer,sizeof(buffer),stdin) != NULL)
    {
        i = strtol(buffer,&p, 10);
        if (buffer[0] != '\n' && (*p =='\n' || *p =='\0') && i>0)
            return (int)i;
        else
            return 0;
    }
    return 0;
}

float getfloat(void)
{
    char buffer[10];
    char *p;
    float i;

    if(fgets(buffer,sizeof(buffer),stdin) != NULL)
    {
        i = strtof(buffer,&p);
        if (buffer[0] != '\n' && (*p =='\n' || *p =='\0') && i>0.0)
            return i;
        else
            return 0;
    }
    return 0;
}
