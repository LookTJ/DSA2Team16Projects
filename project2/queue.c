/**
* This implements the functions for the queues
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "simulation.h"

// both priority and fifo implemented using a linked list.

void SortPriorty(Queue *queuelist)
{
    struct Customer *current, *next;
    int i=0;
    current = queuelist->PriorityHead;
    while(current->nextCust != NULL)
    {
        next = current->nextCust;
        if(current->departureTime != -1 && next->departureTime != -1) //worry about departures
        {
            if(current->departureTime > next->departureTime) //if current departure's time is greater than next
            {
                current->nextCust = next->nextCust;
                next->previousCust = current->previousCust;
                current->previousCust = next;
                next->nextCust = current;
                i++;
            }
        }
        if(current->nextCust == NULL)
            break;
        current = current->nextCust;
    }
    if(i != 0)
        SortPriorty(queuelist);
}

void RemoveFromPriority(Queue *queuelist, struct Customer *link)
{
    struct Customer *next, *previous;
    if(link->previousCust == NULL) //remove head
    {
        if(link->nextCust == NULL) //check if only one left, remove and return.
        {
            queuelist->PriorityHead = NULL;
        }
        else //if not last link
        {
            queuelist->PriorityHead = link->nextCust;
            queuelist->PriorityHead->previousCust = NULL;
        }
    }
    else if(link->nextCust == NULL) //removing last link
    {
        previous = link->previousCust;
        previous->nextCust = NULL;
    }
    else //removing mid link
    {
        previous = link->previousCust;
        next = link->nextCust;
        previous->nextCust = next;
        next->nextCust = previous;
    }
    link->nextCust = NULL;
    link->previousCust = NULL;
}

void RemoveFromFifo(Queue *queuelist)
{
    Customer *temp;
    temp = queuelist->FifoHead;

    if(queuelist->FifoHead->nextCust != NULL)
        queuelist->FifoHead = queuelist->FifoHead->nextCust;
    else
        queuelist->FifoHead = NULL;
    if(queuelist->PriorityHead != NULL)
    {
        temp->nextCust = queuelist->PriorityHead;
        queuelist->PriorityHead->previousCust = temp;
        queuelist->PriorityHead = temp;
        queuelist->PriorityHead->previousCust = NULL;
        SortPriorty(queuelist);
    }
    else
    {
        queuelist->PriorityHead = temp;
        queuelist->PriorityHead->previousCust = NULL;
    }
}
