/**
* This declares all the analytical model calculation functions.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/

#ifndef analytics
#define analytics
/**
 * @brief factorial
 * calculates factorial in a for loop, not a recursive function.
 * @param val
 * the value of which to take the factorial of.
 * @return the n*n-1...*1
 */
unsigned long long factorial(int val); //for loop factorial.
/**
 * @brief summation
 * calculates the sum for part of the percent idle time P0
 * @param M
 * the value for number of server channels
 * @param lambda
 * the value of lamba
 * @param mu
 * value of mu
 * @return the sum of 1/i!(lambda/mu)^i from i=0 to M-1
 */
float summation (int M, float lambda, float mu); //part of percent idle eqn.
/**
 * @brief P0
 * the percent idle analytical calculation
 * @param M
 * the value of M - servers
 * @param lambda
 * the avg arrivals in time
 * @param mu
 * avg number served
 * @return percent idle prediction.
 */
float P0(int M, float lambda, float mu); //percent idle time
/**
 * @brief L
 * calculates the avg number of customers in system.
 * @param M
 * service channels value
 * @param lambda
 * avg arrivals
 * @param mu
 * avg number served
 * @return avg number of customers in the system.
 */
float L(int M, float lambda, float mu); //avg number of people in system.
/**
 * @brief W
 * calculates the avg time customer spends in system
 * @param M
 * service channels
 * @param lambda
 * avg arrivals
 * @param mu
 * avg number served
 * @return the avg time customer spends in system
 */
float W(int M,float lambda, float mu); //avg time customer spends in system
/**
 * @brief Lq
 * calculates avg customers in queue
 * @param M
 * service channels
 * @param lambda
 * avg arrivals
 * @param mu
 * avg number served
 * @return calulates the avg customers in the queue
 */
float Lq(int M,float lambda, float mu); //avg number of customers in queue.
/**
 * @brief Wq
 * calculates the avg time a customer spends in queue.
 * @param M
 * service channel
 * @param lambda
 * avg arrivals
 * @param mu
 * avg number served
 * @return avg time a customer spends in queue.
 */
float Wq(int M,float lambda, float mu); //avg time customer waits in queue
/**
 * @brief rho
 * utilization factor for system
 * @param M
 * service channels
 * @param lambda
 * avg arrivals
 * @param mu
 * avg number served
 * @return utilization factor for the system.
 */
float rho(int M, float lambda, float mu);

#endif
