/**
* This declares the structs and functions for simulation.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#ifndef simulation
#define simulation

typedef struct Customer
{
    float arrivalTime;
    float startofServiceTime;
    float departureTime;
    struct Customer *nextCust;
    struct Customer *previousCust;
} Customer;
typedef struct Servers
{
    int total;
    int available;
} Servers;

typedef struct Statistics
{
    float wait;
    float service;
    int waited;
} Statistics;

/**
 * @brief BuildPriorityQueue
 * This builds a priority queue.
 * @param arrivals
 * numbers of arrivals in system
 * @return a priority queue filled with arrivals.
 */
Customer* BuildPriorityQueue(int arrivals);
/**
 * @brief ProcessNextEvent
 * This function attempts to follow closely to the provided pseudo code.
 * @param queuelist
 * the pointer to the queue.
 * @param curremt_time
 * the pointer to the total time in the system.
 * @param lambda
 * the value for avg arrivals
 * @param Servs
 * the pointer to the struct Servers.
 * @param Stats
 * the pointer to the Statistics struct.
 */
void ProcessNextEvent(struct Queue *queuelist, float *curremt_time, float lambda, struct Servers *Servs, struct Statistics *Stats);
/**
 * @brief BuildServers
 * This builds the the queue of servers.
 * @param queuelist
 * pointer to the queue
 * @param servs
 * pointer to server struct
 * @param lambda
 * value of avg arrivals
 * @param current_time
 * pointer to total time in system.
 */
void BuildServers (struct Queue *queuelist, Servers *servs, float lambda, float *current_time);
#endif
