/**
* This declares a few functions to gather input and a random interval.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#ifndef func
#define func

/**
 * @brief GetNextRandomInterval
 * generates a random time interval based on lambda input
 * @param lambda
 * avg arrivals in system
 * @return a random time interval for functions used in simulation.h
 */
float GetNextRandomInterval(float lambda);
/**
 * @brief getint
 * a function to gather an integer input
 * @return to main with the integer input.
 */
int getint(void);
/**
 * @brief getfloat
 * same as above, except used for floats.
 * @return to main with float input
 */
float getfloat(void);

#endif
