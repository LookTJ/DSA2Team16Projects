/**
* This program runs a queue simulation similiar to that of a bank.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "func.h"
#include "analytics.h"
#include "queue.h"
#include "simulation.h"
#include <time.h>

int main()
{
    time_t seconds;
    seconds = time(NULL);
    srand(seconds);
    
    int n;
    float lambda, mu, *curr_time=malloc(sizeof(float));
    Queue *queuelist = malloc(sizeof(struct Queue*));
    Statistics *Stats = malloc(sizeof(struct Statistics*));
    Servers *Servs = malloc(sizeof(struct Servers*));

    printf("Enter number of arrivals: ");
    while((n = getint()) == 0)
        printf("illegal input! enter an integer: ");
    printf("Enter lambda value: ");
    while((lambda = getfloat()) == 0)
        printf("illegal input! enter a number: ");
    printf("Enter mu value: ");
    while((mu = getfloat()) == 0)
        printf("illegal input! enter a number: ");
    printf("Enter number of servers: ");
    while((Servs->total = getint()) == 0)
        printf("illegal input! enter an integer: ");

    queuelist->PriorityHead = BuildPriorityQueue(n);
    BuildServers(queuelist, Servs, lambda, curr_time);
    ProcessNextEvent(queuelist,curr_time,lambda,Servs,Stats);

    printf("Simulated P0 = %f\n", Stats->service/(*curr_time)); //eqn 1
    printf("Simulated W = %f\n",((Stats->service+Stats->wait)/(float)n)); //eqn 3
    printf("Simulated Wq = %f\n",(Stats->wait/n)); //eqn 5
    printf("Simulated rho = %.4f\n\n",(9*Stats->service/(Servs->total*(*curr_time))));  //eqn 6

    printf("Predicted P0 = %f\n", P0(Servs->total,lambda,mu)); //eqn 1
    printf("Predicted L = %f\n", L(Servs->total,lambda,mu));//eqn 2
    printf("Predicted W = %f\n", W(Servs->total,lambda,mu));//eqn 3
    printf("Predicted Lq = %f\n", Lq(Servs->total,lambda,mu));//eqn 4
    printf("Predicted Wq = %f\n", Wq(Servs->total,lambda,mu)); //eqn 5
    printf("Predicted rho = %f\n\n",rho(Servs->total,lambda,mu)); //eqn 6

    printf("Stats:\n\n");
    printf("Total run time: %f\n",*curr_time);
    printf("total service time: %f\n",Stats->service);
    printf("total wait time: %f\n",Stats->wait);
    printf("Probability (in percentage): %.2f\n", (Stats->waited/(float)n * 100));
    return 0;
}
