/**
* This implements all functions required to calculate the analytical models.
*
* @authors Taylor Lookabaugh, Jose Ramirez
* @date 05 March 2017
* @info Course COP 4534
*/
#include <math.h>
#include "analytics.h"

unsigned long long factorial(int val)
{
    int i;
    unsigned long long fact=1;
    for(i=1;i<=val; ++i)
    {
        fact *= i;
    }
    return fact;
}

float summation (int M, float lambda, float mu)
{
    int i;
    float val = 0;
    for(i=0; i<= M-1; ++i)
    {
        val += (1.0/(float)factorial(i) * powf((lambda/mu),i));
    }
    return val;
}

float P0(int M, float lambda, float mu)
{
    return ((1.0)/(summation(M,lambda,mu) + 1.0/(float)factorial(M) * powf(lambda/mu,(float) M) * ((float)M * mu)/((float)M * mu - lambda)));
}

float L(int M, float lambda, float mu)
{
    return((lambda*mu*powf(lambda/mu,(float)M))/((float)factorial(M-1) * powf((float)M*mu-lambda, 2)) * P0(M,lambda,mu) + (lambda/mu));
}

float W(int M,float lambda, float mu)
{
    return (L(M,lambda,mu)/lambda);
}

float Lq(int M,float lambda, float mu)
{
    return(L(M,lambda,mu)-lambda/mu);
}

float Wq(int M, float lambda, float mu)
{
    return (Lq(M,lambda,mu)/lambda);
}

float rho(int M, float lambda, float mu)
{
    return (lambda/((float)M*mu));
}
